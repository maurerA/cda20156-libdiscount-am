<%@page import="fr.afpa.beans.Utilisateur"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Afficher tous les utilisateurs</title>
</head>
<%ArrayList<Utilisateur>listeUtilisateurs=new ArrayList<Utilisateur>(); %>
<%listeUtilisateurs=(ArrayList<Utilisateur>)request.getAttribute("listeUtilisateur"); %>
<body>

<caption>Affichage de tous les utilisateurs</caption>
<table border="1" cellpadding="5" style="width:100%">

<tr>

<th>nom</th>
<th>nom de la librairie</th>
<th>mail</th>
<th>telephone</th>
<th>supprimer</th>
<th>afficher</th>
<th>modifier</th>
</tr>
<%for(Utilisateur utilisateur1:listeUtilisateurs){ %>

<tr>

 
  <td><%=utilisateur1.getNom() %></td>
  <td><%=utilisateur1.getNomLibrairie() %></td>
  <td><%=utilisateur1.getMail()%></td>
  <td><%=utilisateur1.getTelephone()%></td>
	<td><a href="ServletAfficherSupUtilisateur?id=<%=utilisateur1.getId()%>">Supprimer</a></td>
	<td><a href="ServletAffichUtilisateur?id=<%=utilisateur1.getId()%>">Afficher</a></td>
	<td><a href="ServletAfficherModifSUtilisateur?id=<%=utilisateur1.getId()%>">Modifier</a></td>
	</tr>


<%} %>

</table>

<button onclick="javascript:history.go(-1);">Retour au portail</button>
</body>
</html>