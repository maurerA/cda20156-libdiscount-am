<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Re-Modifier mon profil</title>

</head>
<body>
<% Utilisateur util =(Utilisateur)request.getAttribute("monUtilisateur"); %>

<form action="ServletInscription" method="post" name="ServletAjout">
<label for="nom" >Entrez votre nom</label>
<input type="text" name="nom" placeholder="<%=util.getNom()%>"/>
<p></p>
<label for="prenom">Entrez votre prenom</label>
<input type="text" name="prenom" placeholder= "<%=util.getPrenom()%>" />

<p></p>
<p></p>
<label for="login">Entrez un login pour future connexion</label>
<input type="text" name="login" placeholder="<%=util.getLogin() %>"/>
 <span id="aidePseudo"></span>
<p></p>
<label for="mdp">Entrez votre mot de passe</label>
<input type="password" name="mdp" id="mdp" placeholder="<%=util.getMdp()%>"/>
<span id="aideMdp"></span>
<p></p>
<p></p>
<label for="nomLibrairie">Entrez le nom de votre librairie</label>
<input type="text" name="nomLibrairie" placeholder="<%=util.getNomLibrairie()%>"/>
<p></p>
<p></p>
<label for="nbRue">Entrez le numero de rue de votre librairie</label>
<input type="text" name="nbRue" placeholder="<%=util.getNumRue()%>"/>
<p></p>
<p></p>
<label for="adresse">Entrez la rue de votre librairie</label>
<input type="text" name="nbRue" placeholder="<%=util.getAdresse()%>"/>
<p></p>
<p></p>
<label for="cp">Entrez le code postal de votre librairie</label>
<input type="text" name="cp" placeholder="<%=util.getCodePostal()%>"/>
<p></p>
<p></p>
<label for="cp">Entrez la ville de votre librairie</label>
<input type="text" name="cp" placeholder="<%=util.getVille()%>"/>
<p></p>
<p></p>
<label for="mail">Entrez votre mail</label>
<input type="text" pattern="[^ @]*@[^ @]*" placeholder="Enter votre mail" name="mail" placeholder="<%=util.getMail()%>"/>
<p></p>
<p></p>
<label for="tel">Entrez votre t�l�phone</label>
<input type="text"  name="tel" placeholder="<%=util.getTelephone()%>"/>
<p></p>
<p></p>
<button type="submit" name="bouton" value="Envoyer">Valider</button>
<p></p>
<p></p>

</form></section>
<script src="js.js"></script>
</body>
</html>
</body>
</html>