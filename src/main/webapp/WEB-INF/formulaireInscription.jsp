<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Formulaire d'inscription</title>
</head>
<body>
<body>
<h1>Faisons les présentations!!!!!</h1>
<section>
<form action="ServletInscription" method="post" name="ServletAjout">
<label for="nom">Entrez votre nom</label>
<input type="text" name="nom"/>
<p></p>
<label for="prenom">Entrez votre prenom</label>
<input type="text" name="prenom"/>
<p></p>
<p></p>
<label for="login">Entrez un login pour future connexion</label>
<input type="text" name="login"/>
 <span id="aidePseudo"></span>
<p></p>
<label for="mdp">Entrez votre mot de passe</label>
<input type="password" name="mdp" id="mdp"/>
<span id="aideMdp"></span>
<p></p>
<p></p>
<label for="nomLibrairie">Entrez le nom de votre librairie</label>
<input type="text" name="nomLibrairie"/>
<p></p>
<p></p>
<label for="nbRue">Entrez le numero de rue de la librairie</label>
<input type="text" name="nbRue"/>
<p></p>
<p></p>
<label for="adresse">Entrez l adresse de la librairie</label>
<input type="text" name="adresse"/>
<p></p>
<p></p>
<label for="ville">Entrez la ville de la librairie</label>
<input type="text" name="ville"/>
<p></p>
<p></p>
<label for="cp">Entrez le code postale de la librairie</label>
<input type="text" name="cp"/>
<p></p>
<p></p>
<label for="mail">Entrez votre mail</label>
<input type="text" pattern="[^ @]*@[^ @]*" placeholder="Enter votre mail" name="mail"/>
<p></p>
<p></p>
<label for="tel">Entrez votre téléphone</label>
<input type="text"  name="tel"/>
<p></p>
<p></p>
<div id="secretRole" style="visibility:hidden">
<label for="roleUtilisateur" >role</label>
<input type="text"  name="roleUtilisateur" value="0"/>
</div>
<a href="#" onclick="document.getElementById('secretRole').style.visibility='visible';">&nbsp;&nbsp;</a>
<p></p>
<p></p>
<button type="submit" name="bouton" value="Envoyer">Valider</button>
<p></p>
<p></p>

</form></section>
<script src="js.js"></script>
</body>
</html>