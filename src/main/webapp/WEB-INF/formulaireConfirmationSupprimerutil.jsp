<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Formulaire</title>
<title>Formulaire de confirmation suppression d'annonce</title>
</head>
<body>
<body>
<h1>G�rer les annonces</h1>
<section>
<fieldset>
<legend><b>Supprimer une annonce</b></legend>
<h3>Confirmez-vous la suppression de cette annonce</h3>
<form action="ServletSupprimerUtilisateur" method="post" name="ServletSupprimerUtilisateur">

<table>
	
	<tr>
		<td><label for="nom">Nom</label></td>
		<td>${monUtilisateur.nom}</td>
	</tr>
	<tr>
		<td><label for="prenom">prenom</label></td>
		<td>${monUtilisateur.prenom}</td>
	</tr>
	<tr>
		<td><label for="login">login</label></td>
		<td>${monUtilisateur.login}</td>
	</tr>
	<tr>
		<td><label for="mot de passe">mot de passe</label></td>
		<td>${monUtilisateur.mdp}</td>
	</tr>
	<tr>
		<td><label for="nom de la librairie">nom de la librairie</label></td>
		<td>${monUtilisateur.nomLibrairie}</td>
	</tr>
	<tr>
		<td><label for="numero de la rue">numero de la rue</label></td>
		<td>${monUtilisateur.numRue}</td>
	</tr>
	<tr>
		<td><label for="adresse">adresse</label></td>
		<td>${monUtilisateur.adresse}</td>
	</tr>
	<tr>
		<td><label for="code postal">code postal</label></td>
		<td>${monUtilisateur.codePostal}</td>
	</tr>
	<tr>
		<td><label for="ville">ville</label></td>
		<td>${monUtilisateur.ville}</td>
	</tr>
	<tr>
		<td><label for="mail">mail</label></td>
		<td>${monUtilisateur.mail}</td>
	</tr>
	<tr>
		<td><label for="qtt">telephone</label></td>
		<td>${monUtilisateur.telephone }</td>
	</tr>
	</table>
    <input type="hidden" name="id" value="${monUtilisateur.id}"/> 
	<input type="hidden" name="roleUtilisateur" value="${monUtilisateur.roleUtilisateur}"/> 
<button type="submit" name="bouton" value="Envoyer">Valider</button>
</form>
<p></p>
</fieldset>
<p></p>
  <form action="ServletAfficherPortail" method="post" name="servletAfficherPortail">                 
	<button type="submit" name="afficherPortail" value="Afficher">Retour Portail</button>
</form>

</section>

</body>
</html>