package fr.afpa.DAO;

import java.util.ArrayList;

import fr.afpa.beans.Utilisateur;

public interface IDAO {
	//------------------------------------PARTIE UTILISATEUR---------------------------------//
	//creation compte utilisateur
	public boolean createIfNotExist(Utilisateur utilisateur);
	//verifier la bonne connexion et recuperer utilisateur
	public Utilisateur recupereUtilisateur(Utilisateur utilisateur);
	//pour le super Utilisateur : afficher ts les utlisateurs
	public ArrayList<Utilisateur> afficherTousUtilisateurs();
	//faire une recherche d'un utilisateur
	public Utilisateur afficherUnUtilisateur(Utilisateur utilisateur);
	// pour le super Utilisateur : supprimer un utlisateur
	public boolean deleteUtilisateur(Utilisateur utilisateur);
	// mis a jour des infos utilisateurs par l utilisateur titulaire du compte
	public Utilisateur misAJourUtilisateur(Utilisateur utilisateur);
}
