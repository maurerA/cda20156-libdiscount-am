package fr.afpa.DAO;

import java.util.ArrayList;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Utilisateur;
import session.HibernateUtils;

public class DAOGestionUtilisateur implements IDAO {
	/**
	 * methode permettant de verifier si le code ou le mot de passe existe
	 * pour une creation de compte
	 */
	@Override
	public boolean createIfNotExist(Utilisateur utilisateur) {
		// Creation d une session
		Session s ;
		//Creation d une session hibernate
		s= HibernateUtils.getSession();
		//Debut de la transaction
		Transaction tx=s.beginTransaction();
		Query q = s.getNamedQuery("Verif_utilisateur");
		q.setParameter("login", utilisateur.getLogin());
		q.setParameter("mdp",utilisateur.getMdp());
		//Enregistrement de l objet Utilisateur
		if (q.getResultList().size()!=0) {
			
			return false;
		}

		s.save(utilisateur);
		tx.commit();

		return true;
	}
/**
 * methode permettant de verifier si le login et le mdp sont correctes
 * pour une verif de compte
 */
	@Override
	public Utilisateur recupereUtilisateur(Utilisateur utilisateur) {
		// Creation d une session
				Session s ;
				//Creation d une session hibernate
				s= HibernateUtils.getSession();
				//Debut de la transaction
				Transaction tx=s.beginTransaction();
				Query q = s.getNamedQuery("Verif_utilisateur");
				q.setParameter("login", utilisateur.getLogin());
				q.setParameter("mdp",utilisateur.getMdp());
				
				//Enregistrement de l objet Utilisateur
				if (q.getResultList().size()==1) {
					
					//Utilisateur util = s.get(Utilisateur.class,(int) q.getResultList().get(0));
					
					Utilisateur util =((Utilisateur) q.getResultList().get(0));
					
					return util;
				}

				
				tx.commit();

				return null;
		
	}
	/**
	 * Methode pour afficher dans la vue ts les utilisateurs inscrits
	 */
@Override
public ArrayList<Utilisateur> afficherTousUtilisateurs() {
	
	Session s ;
	
	//Creation d une session hibernate
	s= HibernateUtils.getSession();
	
	//Debut de la transaction
	Transaction tx=s.beginTransaction();
	Query q = s.getNamedQuery("dysplayAlls_utilisateur");
	
	
	ArrayList<Utilisateur> listeDePersonne = new ArrayList<Utilisateur>();
listeDePersonne = (ArrayList<Utilisateur>)q.getResultList();
if (listeDePersonne.size()==0) {
	return null;
}
	
	tx.commit();
	s.close();
	
	return listeDePersonne;
}

/**
 * Methode pour supprimer un utilisateur
 */
@Override
public boolean deleteUtilisateur(Utilisateur utilisateur) {
	
	Session s ;
	
	//Creation d une session hibernate
	s= HibernateUtils.getSession();
	//Debut de la transaction
	Transaction tx=s.beginTransaction();
	Query q = s.getNamedQuery("update_utilisateur");
	q.setParameter("id",utilisateur.getId());
	
	ArrayList<Utilisateur>listeDePersonnes = null;
	
	
	listeDePersonnes= (ArrayList<Utilisateur>)q.getResultList();	
	//Enregistrement de l objet Utilisateur
	
	if (listeDePersonnes.size()==0) {
		
		return false;
	}else {
	

Utilisateur util = listeDePersonnes.get(0);
	s.delete(util);
	tx.commit();
	s.close();
	
	
	return true;}
}

/**
 * requete et traitement pour la mise a jour d un utilisateur dans la base de donnee
 */
@Override
public Utilisateur misAJourUtilisateur(Utilisateur utilisateur) {		
		
		Session s ;
		
		//Creation d une session hibernate
		s= HibernateUtils.getSession();
		//Debut de la transaction
		Transaction tx=s.beginTransaction();
		//recuper la personne
		Query q = s.getNamedQuery("update_utilisateur");
		q.setParameter("id", utilisateur.getId());
		
		
		Utilisateur util = null;
	try {
		util = (Utilisateur) q.getSingleResult();
	} catch (NoResultException e) {
		return null;
	}

	if (util!=null) {
	//renseignement des champs pour la modif du profil
		util.setNom(utilisateur.getNom());;
		util.setPrenom(utilisateur.getPrenom());
		util.setLogin(utilisateur.getLogin());
		util.setMdp(utilisateur.getMdp());
		util.setNomLibrairie(utilisateur.getNomLibrairie());
		util.setAdresse(utilisateur.getAdresse());
		util.setCodePostal(utilisateur.getCodePostal());
		util.setVille(utilisateur.getVille());
		util.setNumRue(utilisateur.getNumRue());
		util.setMail(utilisateur.getMail());
		util.setPrenom(utilisateur.getTelephone());
	//mise a jour de bdd	
		s.update(util);
		tx.commit();
		s.close();
		return util;
	}
	return null;
		
}
@Override
public Utilisateur afficherUnUtilisateur(Utilisateur utilisateur) {
			
	Session s ;
	
	//Creation d une session hibernate
	s= HibernateUtils.getSession();
	
	//Debut de la transaction
	Transaction tx=s.beginTransaction();
	Query q = s.getNamedQuery("update_utilisateur");
	//mettre l id dans la requete
	q.setParameter("id", utilisateur.getId());
	
	Utilisateur util = null;
	try {
		util = (Utilisateur) q.getSingleResult();
	} catch (NoResultException e) {
		return null;
	}

			tx.commit();
			s.close();
			return util;
			
		
	}



}
