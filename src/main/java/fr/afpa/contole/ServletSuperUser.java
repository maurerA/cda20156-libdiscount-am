package fr.afpa.contole;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletSuperUser
 */
public class ServletSuperUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletSuperUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//methode qui recupere le login ecrit par l utilisateur
				String login = request.getParameter("login");
				//methode qui prende le password de l utilisateur
				String password = request.getParameter("password");
				//methode qui recupere avec l aide du login de l utilisateur le mot de passe de l utilisateur(il scann chaque element mis dans le web.xml
				//de facon general il recupere la valeur du param name
				String paramLogin = getServletConfig().getInitParameter(login);
				System.out.println(paramLogin);
				
				String orientation="Index.html";
				if (paramLogin != null && password.equals(paramLogin)) {
					
					orientation=IPath.SUPER_USER;
					
				}else {
					orientation="index.html";
				}
				//permet d orienter le flux vers une nouvelle page web
				RequestDispatcher dispacher = request.getRequestDispatcher(orientation);
				dispacher.forward(request, response);
	}

}
