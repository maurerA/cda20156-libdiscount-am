package fr.afpa.contole;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metier.GestionUtilisateur;
import fr.afpa.metier.IGestion;

/**
 * Servlet implementation class ServletAffichUtilisateur
 */
public class ServletAffichUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAffichUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String direction="index.jsp";
		if(id!=null)
		{
		
			//récupération des informations de l'utilisateur
			IGestion recupProfil = new GestionUtilisateur();
			Utilisateur profil = new Utilisateur();
			profil.setId(Integer.parseInt(id));
			Utilisateur afficherProfil = recupProfil.afficherOnlyOneUtilisateur(profil);
			request.setAttribute("monProfil", afficherProfil);
			//afficher la page du profil demande
			direction = IPath.ADMIN_AFFI_UTILISATEUR;
		}
		
			
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(direction);
		dispatcher.forward(request, response);
		
	}

}
