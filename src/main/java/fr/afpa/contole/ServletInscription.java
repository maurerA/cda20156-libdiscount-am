package fr.afpa.contole;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Utilisateur;
import fr.afpa.metier.GestionUtilisateur;
import fr.afpa.metier.IGestion;


/**
 * Servlet implementation class ServletInscription
 * Pour la creation de compte
 */
public class ServletInscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletInscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * controle et renvoie aux differentes pages lors de la création de compte
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//boolean de verif
		boolean verif;
		//boolean de creation compte ok
		boolean ok;
		//recuperation des elements du formulaire
				String nom = request.getParameter("nom");
				String prenom = request.getParameter("prenom");
				String login =request.getParameter("login");
				String motDePasse = request.getParameter("mdp");
				String nomLib =request.getParameter("nomLibrairie");
				String mail = request.getParameter("mail");
				String tel = request.getParameter("tel");
				int nbRue= Integer.parseInt(request.getParameter("nbRue"));
				String adresse= request.getParameter("adresse");
				int cp = Integer.parseInt(request.getParameter("cp"));
				String ville = request.getParameter("ville");
				int role = Integer.parseInt(request.getParameter("roleUtilisateur"));
				
		//instanciation de l'utilisateur
				Utilisateur utilisateur = new Utilisateur(); ;				
				utilisateur.setNom(nom);
				utilisateur.setPrenom(prenom);
				utilisateur.setLogin(login);;
				utilisateur.setMdp(motDePasse);
				utilisateur.setNomLibrairie(nomLib);
				utilisateur.setMail(mail);
				utilisateur.setTelephone(tel);
				utilisateur.setAdresse(adresse);
				utilisateur.setCodePostal(cp);
				utilisateur.setVille(ville);
				utilisateur.setNumRue(nbRue);
				utilisateur.setRoleUtilisateur(role);
		//instanciation de la couche metier utilisateur
				IGestion creaCompte=new GestionUtilisateur();
		//verification des saisies
				IControlUtilisateur controlUtil = new Controlutilisateur();
		//verification si les champs remplis sont nulls ou pas		
				verif=controlUtil.controlNotNull(utilisateur);
		//Creation d une variable contenant le chemin de retour au cas ou
				String direction = "index.jsp";
				if (verif) {
					//si formulaire bien rempli, l utilisateur est envoye a la couche metier  
					ok=creaCompte.creationCompte(utilisateur);
					//verification si le compte est daja utilise ou non
					if (ok) {
						//si le compte est daja utilise
						direction=IPath.INSCRIPTION_REUSSIE;
					}else {
						//si le compte est bien créé
						direction = IPath.LOGIN_DEJA_UTILISE;
					}
				}else {
					//le formulaire n est pas rempli correctement, direction vers une page d alerte
					direction = IPath.FORMULAIRE_KO;
					
				}
	
				RequestDispatcher dispatcher = request.getRequestDispatcher(direction);
				dispatcher.forward(request, response);
				
				
					

	}

}
