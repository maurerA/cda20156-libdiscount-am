package fr.afpa.contole;

public interface IPath {
public final String DONNEES_PERSO_UTIL="/WEB-INF/donnéesPersoUtilisateur.jsp",
					FORMULAIRE_INSCRIPTION="/WEB-INF/formulaireInscription.jsp",
					FORMULAIRE_KO="/WEB-INF/formulaireKO.html",
					IMPORT_REUSSI="/WEB-INF/importreussi.html",
					INSCRIPTION_REUSSIE="/WEB-INF/inscriptionReussie.html",
					LOGIN_DEJA_UTILISE="/WEB-INF/loginDejaUtilise.html",
					MDP_LOGIN_KO="/WEB-INF/mdpOuLoginIncorrect.html",
					PORTAIL_UTILISATEUR="/WEB-INF/portailUtilisateur.jsp",
					SUPER_USER="/WEB-INF/supperUser.jsp",
					AFFICHER_ALLS_UTIL="/WEB-INF/afficherTsLesUtilisateurs.jsp",
					MODIFIER_MON_PROFIL="/WEB-INF/modifierMonProfil.jsp",
					EFFACE_UN_UTILISATEUR="/WEB-INF/EffacerUnUtilisateur.jsp",
					AFFICHER_ONE_UTIL="/WEB-INF/TrouverUnUtilisateur.jsp",
					SUP_OK="/WEB-INF/suppresionReussie.html",
					SUP_KO="/WEB-INF/suppressionKO.html",
					ADMIN_AFFI_UTILISATEUR="/WEB-INF/AdminAffUtilisateur.jsp",
					FORMULAIRE_CONFIRMATION_SUP_UTIL="/WEB-INF/formulaireConfirmationSupprimerutil.jsp"
					
;

}
