package fr.afpa.contole;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metier.GestionUtilisateur;
import fr.afpa.metier.IGestion;

/**
 * Servlet implementation class ServletAfficherSupUtilisateur
 */
public class ServletAfficherSupUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAfficherSupUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//récupération paramètre
				String idParameter= request.getParameter("id");
				
				if(idParameter!=null)
				{
				
					//récupération des information de l'annonce
					IGestion gestUtil = new GestionUtilisateur();
					Utilisateur utilisateurParam = new Utilisateur();
					utilisateurParam.setId(Integer.parseInt(idParameter));
					Utilisateur util = gestUtil.afficherOnlyOneUtilisateur(utilisateurParam);
					
					request.setAttribute("monUtilisateur", util);
				
				}
				
				//direction
				String direction =IPath.FORMULAIRE_CONFIRMATION_SUP_UTIL;

				RequestDispatcher dispatcher = request.getRequestDispatcher(direction);
				dispatcher.forward(request, response);
									

			}

}
