package fr.afpa.contole;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Utilisateur;
import fr.afpa.metier.GestionUtilisateur;
import fr.afpa.metier.IGestion;

/**
 * Servlet implementation class ServletIdentification
 */
public class ServletIdentification extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletIdentification() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Varialble boolean pour gerer la direction si la connexion existe ou pas
		boolean connexionExist;
		//affectation d une variable par defaut si probleme de redirection
		String direction = "index.jsp";
		//recuperation des donnees login et password de l utilisateur
		String login =request.getParameter("login");
		String motDePasse = request.getParameter("mdp");
		
		//instanciation de l objet appartenant a la class Utilisateur
		
		Utilisateur utilisatateur = new Utilisateur();
		utilisatateur.setLogin(login);
		utilisatateur.setMdp(motDePasse);
		//Instanciation de la couche metier
		IGestion gestUtil = new GestionUtilisateur();
		
		//redirection vers le portail utilisateur si le mot de passe et login correcte
		Utilisateur util=gestUtil.verifConnexion(utilisatateur);
		if (util!=null) {
			//recuperer la session web ou la creer
			HttpSession session = request.getSession(true);
			//Alimenter des attributs de la session
			session.setAttribute("login",util.getLogin());
			session.setAttribute("idUtilisateur",util.getId());
			session.setAttribute("utilisateurConnecte",util);
			request.setAttribute("numRole",util.getRoleUtilisateur());
			direction = IPath.PORTAIL_UTILISATEUR;
			
		}else {
			direction=IPath.MDP_LOGIN_KO;
	
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(direction);
		dispatcher.forward(request, response);
	}
	

}
