package fr.afpa.contole;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Utilisateur;
import fr.afpa.metier.GestionUtilisateur;
import fr.afpa.metier.IGestion;

/**
 * Servlet implementation class ServletActionsUtilisateur
 */
public class ServletActionsUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletActionsUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
		//Recuperer des actions de la page portailUtilisateur
		String modif = request.getParameter("modif");
		String affichAlls = request.getParameter("affichAlls");
		String affichOne =request.getParameter("affichOne");
		HttpSession session=request.getSession();
		//recuperer l utilisateur dans la session ouverte
		Utilisateur utilisateur=(Utilisateur)session.getAttribute("utilisateurConnecte");
		//instanciation de la couche metier
		IGestion metier = new GestionUtilisateur();
		//direction par defaut
		String direction="index.jsp";
		if("modifier mes donnees".equals(modif)) {
			
			
			//affecter l utilisateur pour la prochaine servlet
			request.setAttribute("monUtilisateur", utilisateur);
			direction=IPath.MODIFIER_MON_PROFIL;
		}else if ("Afficher tous les utilisateurs".equals(affichAlls) && utilisateur.getRoleUtilisateur()!=1) {
			listeUtilisateurs=metier.afficherAllsUtilisateurs();
			request.setAttribute("listeUtilisateur",listeUtilisateurs);
			direction=IPath.AFFICHER_ALLS_UTIL;
		}else if ("Afficher un utilisateur".equals(affichOne)) {
			direction=IPath.AFFICHER_ONE_UTIL;
		}
				//oriente le flux vers la bonne page
		
				RequestDispatcher dispatcher = request.getRequestDispatcher(direction);
		dispatcher.forward(request, response);
	}

}
