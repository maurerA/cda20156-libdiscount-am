package fr.afpa.contole;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.DAO.IDAO;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metier.GestionUtilisateur;
import fr.afpa.metier.IGestion;
import net.bytebuddy.agent.builder.AgentBuilder.InitializationStrategy.Dispatcher;

/**
 * Servlet implementation class ServletSupprimerUtilisateur
 */
public class ServletSupprimerUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletSupprimerUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//chemin par defaut
		String direction="index.jsp";
		//preparation de la variable bolean pour le retour de la methode
		boolean verif;
		//recuperation de l id de la personne a supprimer
		String id = request.getParameter("id");
		//recuperation du role de l utilisateur
		String role = request.getParameter("roleUtilisateur");
		int rol = Integer.parseInt(role);
		if (id.isEmpty()||rol==0) {
			direction=IPath.SUP_KO;
			RequestDispatcher dispatcher = request.getRequestDispatcher(direction);
			dispatcher.forward(request, response);		
			}else if (rol==1) {
				
			
		
		//instanciation de l objet metier
		IGestion metier = new GestionUtilisateur();
		//instanciation de l objet utilisateur
		Utilisateur util = new Utilisateur();
		//affectation a l objet utilisateur de la valeur de l id
		util.setId(Integer.parseInt(id));
		
		//appel de la methode suprimer 
		verif = metier.effacerUtilisateur(util);
		
		if (verif) {
			//si verif ok
			direction=IPath.FORMULAIRE_CONFIRMATION_SUP_UTIL;
		}else {
			direction=IPath.SUP_KO;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(direction);
		dispatcher.forward(request, response);}
	}}


