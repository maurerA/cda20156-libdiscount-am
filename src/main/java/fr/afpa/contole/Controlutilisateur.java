package fr.afpa.contole;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;

import fr.afpa.beans.Utilisateur;

public class Controlutilisateur implements IControlUtilisateur {

	@Override
	public boolean controlNotNull(Utilisateur utilisateur) {
		 
			if (utilisateur.getNom()==""||utilisateur.getNom().length()<2||utilisateur.getNom().length()>20) {
				return false;
			} else if(utilisateur.getPrenom()==""||utilisateur.getPrenom().length()<2||utilisateur.getPrenom().length()>20) {
				return false;
			
			}else if(utilisateur.getMail()=="") {
				return false;
			}else if(utilisateur.getNomLibrairie()=="") {
				return false;
			}else if(utilisateur.getNumRue()==0) {
				return false;
			}else if (utilisateur.getAdresse()=="") {
				return false;
			}else if (utilisateur.getCodePostal()==0) {
				return false;
			}else if (utilisateur.getVille()=="") {
				return false;
			}else if(utilisateur.getTelephone()=="") {
				return false;
			}
			return true;
			
	}

	
}
