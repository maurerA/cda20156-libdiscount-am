package fr.afpa.metier;

import java.util.ArrayList;

import fr.afpa.DAO.DAOGestionUtilisateur;
import fr.afpa.DAO.IDAO;
import fr.afpa.beans.Utilisateur;

public class GestionUtilisateur implements IGestion{

	@Override
	public boolean creationCompte(Utilisateur utilisateur) {
		//variable pr recuperer le retour de la methode
		boolean verif;
		//instanciation de l interface couche DAO
		IDAO verifLoginExist = new DAOGestionUtilisateur();
		//control si l'utilisateur existe
		verif=verifLoginExist.createIfNotExist(utilisateur);
		if (verif) {
			return true;
		}
		return false;
	}

	@Override
	public Utilisateur verifConnexion(Utilisateur utilisateur) {
		//instanciation de l objet de la class DAO
		IDAO verifConn = new DAOGestionUtilisateur();
		Utilisateur util=verifConn.recupereUtilisateur(utilisateur);
		
		return util;
	}

	@Override
	public ArrayList<Utilisateur> afficherAllsUtilisateurs() {
		IDAO afficherUtilisateurs = new DAOGestionUtilisateur();
		ArrayList<Utilisateur>listeUtilisateurs=afficherUtilisateurs.afficherTousUtilisateurs();
		return listeUtilisateurs;
	}

	@Override
	public boolean effacerUtilisateur(Utilisateur utilisateur) {
	boolean verif;
		IDAO effUtilisateur = new DAOGestionUtilisateur();
		verif = effUtilisateur.deleteUtilisateur(utilisateur);
		if (verif) {
			return true;
		}
		return false;
	}

	@Override
	public Utilisateur updateInfosUtilisateur(Utilisateur utilisateur) {
		IDAO miseAJourUtilisateur = new DAOGestionUtilisateur();
		Utilisateur util = miseAJourUtilisateur.misAJourUtilisateur(utilisateur);
		return util;
	}

	@Override
	public Utilisateur afficherOnlyOneUtilisateur(Utilisateur utilisateur) {
		IDAO afficherUnUtilisateur = new DAOGestionUtilisateur();
		Utilisateur util = afficherUnUtilisateur.afficherUnUtilisateur(utilisateur);
		return util;
	}

	

	
}
