package fr.afpa.metier;

import java.util.ArrayList;

import fr.afpa.beans.Utilisateur;

public interface IGestion {
		
//------------------------------------PARTIE UTILISATEUR---------------------------------//
		//creation compte utilisateur
	public boolean creationCompte(Utilisateur utilisateur);
		//verifier la bonne connexion et recuperer utilisateur
	public Utilisateur verifConnexion(Utilisateur utilisateur);
		//pour le super Utilisateur : afficher ts les utlisateurs
	public ArrayList<Utilisateur> afficherAllsUtilisateurs();
		
	public Utilisateur afficherOnlyOneUtilisateur(Utilisateur utilisateur);
	// pour le super Utilisateur : supprimer un utlisateur
	public boolean effacerUtilisateur(Utilisateur utilisateur);
		// mis a jour des infos utilisateurs
	public Utilisateur updateInfosUtilisateur(Utilisateur utilisateur);
	
	
	//------------------------------------PARTIE ANNONCE---------------------------------//
	
}
