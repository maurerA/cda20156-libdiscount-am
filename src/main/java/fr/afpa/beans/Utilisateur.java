package fr.afpa.beans;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor

@Entity
@NamedQuery(name="dysplayAlls_utilisateur",query=" FROM Utilisateur u")
@NamedQuery(name="Verif_utilisateur",query=" FROM Utilisateur u where u.login=:login or u.mdp=:mdp")
@NamedQuery(name = "update_utilisateur",query="From Utilisateur u where u.id=:id")
public class Utilisateur {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "utilisateur_id_generator")
	@SequenceGenerator(name = "utilisateur_id_generator", sequenceName = "utilisateur_seq", allocationSize = 1, initialValue = 1)
	@Column(name = "id_utilisateur")
	private int id;
	@NonNull
	@Column(nullable=false)
	private String nom;
	@NonNull
	@Column(nullable=false)
	private String prenom;
	@NonNull
	@Column(nullable=false)
	private String login;
	@NonNull
	@Column(nullable=false)
	private String mdp;
	@NonNull
	@Column(nullable=false)
	private String nomLibrairie;
	@NonNull
	@Column(nullable=false)
	private int numRue;
	@NonNull
	@Column(nullable=false)
	private String adresse;
	@Column(nullable=false)
	private int codePostal;
	@NonNull
	@Column(nullable=false)
	private String ville;
	@NonNull
	@Column(nullable=false)
	private String mail;
	@NonNull
	@Column(nullable=false)
	private String telephone;
	@NonNull
	@Column(nullable=false)
	private int roleUtilisateur=1;
//	@NonNull
//	@Column(nullable = false)
//	private int role;
	//@OneToMany (mappedBy = "utilisateur",fetch = (FetchType.EAGER), cascade =  CascadeType.ALL)
	//@JoinColumn (name="fk_id_utilisateur")
	//private Set<Annonce> annonce;
	//mappedBy = "utilisateur",
	

	
}