package fr.afpa.beans;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor


@Entity
public class Annonce {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "annonce_id_generator")
	@SequenceGenerator(name = "annonce_id_generator", sequenceName = "annonce_seq", allocationSize = 1, initialValue = 1)
	@Column(name = "id_annonce")
	private int id;
	private String titre;
	private String NivScolR;
	private int ISBN;
	private LocalDate dateEdition;
	private String  maisonEdit;
	private float  PU;
	private int qtt; 
	private int remise;
	@Transient
	private float pTTC;
	private boolean active=true;
//	@OneToMany (mappedBy = "emetteur",fetch = (FetchType.EAGER), cascade =  CascadeType.ALL)
//	private Set<Photo> photo;
	@ManyToOne (fetch = (FetchType.EAGER), cascade =  CascadeType.ALL)
//	@JoinColumn (name="fk_id_utilisateur", referencedColumnName = "id_utilisateur")
	Utilisateur utilisateur;
	
	public float getpTTC() {
		return this.qtt*this.PU*(1-this.remise);
	}
	
	
	
	
	
	
}

